SOURCE := cv.md 
HTML := public/index.html
TEMPLATE := default.html4

.PHONY : all
all: $(HTML)

$(HTML) : $(SOURCE) $(TEMPLATE)
	@echo --- Generating HTML ---
	@pandoc -s \
		-f markdown \
		-t html \
		--template $(TEMPLATE) \
		--lua-filter multibib.lua \
		--lua-filter highlight_author.lua \
		-o $@ \
		$< 

.PHONY : clean
clean :
	@echo --- Deleting generated files ---
	@-rm $(HTML) 
