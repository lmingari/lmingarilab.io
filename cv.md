---
title: Resume
subtitle: Postdoctoral Research Fellow
author: Leonardo Mingari
lang: en
bibliography: 
    papers: my_papers.bib
    posters: my_posters.bib
csl: apa-cv.csl
nocite: '@*'
css:
- https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css
- https://fonts.googleapis.com/css?family=Cambria
- styling.css
include-before: <div class="resume-main">
include-after: </div>
---

::: left-box
## Summary
Researcher with a strong background in Physics and Atmospheric Sciences, communication skills, extensive teaching experience and ability to work independently and in a team environment.

## Contact
* []{.fa .fa-map-marker} Barcelona, Spain
* []{.fa .fa-mobile-phone} (+34) 658 41 51 61
* []{.fa .fa-envelope} [lmingari@geo3bcn.csic.es](mailto:lmingari@geo3bcn.csic.es)
* []{.fa .fa-gitlab} [lmingari](https://gitlab.com/lmingari)
* []{.fa .fa-github} [lmingari](https://github.com/lmingari)

## Skills

* Programming languages: Modern fortran, MPI, Python, Latex, Haskell, Java, C/C++ (basic)
* Unix/Linux environment: Unix Shell Scripting, Git, Vim
* Scientific tools: NCAR Command Language (NCL), Numpy, Pandas, Xarray, Cartopy, QGIS, Inkscape
* Data formats: GRIB, NetCDF, HDF5

## Languages

* Spanish: Native
* English: B2
:::

::: right-box
# Leonardo Mingari {.title}
# Postdoctoral Research Fellow {.subtitle}

## Research Experience
::: item
- 2023--Present
- Geosciences Barcelona (Barcelona, Spain)
- Postdoctoral researcher
- Project: Centre of Excellence for Exascale in Solid Earth - 2nd phase <br>
  Project: DT-GEO

- 2019--2021
- Barcelona Supercomputing Center (Barcelona, Spain)
- Recognised Researcher R2
- Project: Centre of Excellence for Exascale in Solid Earth

- 2012--2013
- National Weather Service (Argentina)
- Undergraduate Researcher
- Project: Application of modern Numerical Models for weather forecasting at the National Meteorological Service, Argentina. Studies of environmental vulnerability and socio-economic impact
:::

## Education
:::item
* 2018
* PhD in Earth & Environmental Sciences
* University of Buenos Aires (Argentina)
* Distinction: Outstanding thesis

* 2012
* University Degree in Physics
* University of Buenos Aires (Argentina)
* Grade: 9.04/10
:::

## Leadership Experience
:::item
* 2022--Present
* Supervisor of doctoral thesis
* University of Buenos Aires (Argentina)
* Title: Pronósticos por ensambles de removilización de ceniza volcánica en el sur de Sudamérica <br>
  Candidate: Vazquez, E. <br>
:::

## Publications
::: {#refs_papers}
:::

## Selected Poster Presentations
::: {#refs_posters}
:::

:::
